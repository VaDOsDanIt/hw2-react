import React, {Component} from 'react';
import Card from './card'

class CardList extends Component {
    constructor(props){
        super(props);
    }

    render() {
        const cards = this.props.cards.map(item => {
            return(
                <Card key={item.article} card={item}/>
            )
        })

        return (
                <div className="d-flex flex-wrap col-12">
                    {cards}
                </div>
        );
    }
}

export default CardList;