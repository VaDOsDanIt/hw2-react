import React, {Component} from 'react';
import Button from './Button'

class Modal extends Component {
    constructor(props) {
        super(props);
        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    render() {
        const {header, closeButton, text, actions} = this.props.modal;

        const btns = actions.map(item => <Button handleClick={item.handleClick} text={item.text}/>);

        const clsButton = closeButton && <a onClick={event => {
            event.preventDefault();
            this.props.handleClick();
        }} href="#" className="btn-closeModal"> </a>;
        return (
            <div>
                <div className='modal-container'>
                    <div ref={this.setWrapperRef} className="modal_">
                        <div className="modal-header">
                            <p className="title">{header}</p>
                            {clsButton}
                        </div>
                        <div className="modal-body">
                            <p className="text">
                                {text}
                            </p>
                        </div>
                        <div className="modal-footer">
                            {btns}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    setWrapperRef(node) {
        this.wrapperRef = node;
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside(event) {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.props.handleClick();
            console.log("outside");
        }
    }
}

export default Modal
