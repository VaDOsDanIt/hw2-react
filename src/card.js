import React, {Component} from 'react';
import Modal from './Modal'

class Card extends Component {
    constructor(props) {
        super(props);
        this.likeHandler = this.likeHandler.bind(this);
        this.cardHandler = this.cardHandler.bind(this);
        this.handleModal = this.handleModal.bind(this);
        this.state = {
            isOpenModal: false,
            isLiked: false,
            isCard: false,

        }
    }

    componentDidMount() {
        this.setState((prevState, props) => ({
            isLiked: JSON.parse(localStorage.getItem(`like_${props.card.article}`)),
            isCard: JSON.parse(localStorage.getItem(`card_${props.card.article}`))
        }));

    }

    likeHandler(evt) {
        evt.preventDefault();
        this.setState((prevState, props) => {
            prevState.isLiked ? localStorage.removeItem(`like_${props.card.article}`) :
                localStorage.setItem(`like_${props.card.article}`, JSON.stringify(!prevState.isLiked));
            return ({
                isLiked: !prevState.isLiked,
            })
        });
    }

    cardHandler(evt) {
        evt.preventDefault();
        this.handleModal();
        this.setState((prevState, props) => {
            prevState.isCard ? localStorage.removeItem(`card_${props.card.article}`) :
                localStorage.setItem(`card_${props.card.article}`, JSON.stringify(!prevState.isCard));
            return ({
                isCard: !prevState.isCard,
            })
        });
    }

    handleModal(evt) {
        this.setState((prevState, props) => {
            return ({
                isOpenModal: !prevState.isOpenModal,
            })
        })
    }

    render() {
        const firstModal = {
            header: "Вы хотите добавить данный товар в корзину?",
            closeButton: true,
            text: `Товар: ${this.props.card.name}`,
            actions: [{
                text: "Отменить",
                handleClick: this.handleModal,
            },
                {
                    text: "Добавить",
                    handleClick: this.cardHandler,
                }
            ]
        }

        const secondModal = {
            header: "Вы хотите удалить данный товар из корзины?",
            closeButton: true,
            text: `Товар: ${this.props.card.name}`,
            actions: [{
                text: "Отменить",
                handleClick: this.handleModal,
            },
                {
                    text: "Удалить",
                    handleClick: this.cardHandler,
                }
            ]
        }
        const {name, price, urlImg, article, color} = this.props.card;
        const likeBtn = this.state.isLiked ?
            <a onClick={this.likeHandler} className="text-danger text-decoration-none" href="#">&#9825;</a> :
            <a onClick={this.likeHandler} className="text-dark text-decoration-none" href="#">&#9825;</a>;

        const card = this.state.isCard ?
            <button onClick={this.handleModal} className="btn btn-success"><img width='30px'
                                                                                src="https://pngimg.com/uploads/shopping_cart/shopping_cart_PNG38.png"
                                                                                alt="card"/></button> :
            <button onClick={this.handleModal} className="btn btn-secondary"><img width='30px'
                                                                                  src="https://pngimg.com/uploads/shopping_cart/shopping_cart_PNG38.png"
                                                                                  alt="card"/></button>;


        let modal;
        if (this.state.isOpenModal) {
            if (this.state.isCard) {
                modal = <Modal modal={secondModal}/>;
            } else {
                modal = <Modal modal={firstModal}/>;
            }
        }

        return (
            <ul className="card col-12 col-sm-4 col-lg-3 w-25 p-2 d-flex justify-content-center flex-column align-items-center">
                {modal}
                <img className="p-1" height="300px" src={urlImg} alt="img"/>

                <table>
                    <tr>
                        <td className="text-center w-50 p-1">Model:</td>
                        <td className="text-center w-50 p-1">{name}</td>
                    </tr>
                    <tr>
                        <td className="text-center w-50 p-1">Price:</td>
                        <td className="text-center w-50 p-1">{price}</td>
                    </tr>
                    <tr>
                        <td className="text-center w-50 p-1">Article:</td>
                        <td className="text-center w-50 p-1">{article}</td>
                    </tr>
                    <tr>
                        <td className="text-center w-50 p-1">Color:</td>
                        <td className="text-center w-50 p-1">{color}</td>
                    </tr>
                    <tr>
                        <td className="text-center w-50 p-1">
                            {card}
                        </td>
                        <td className="text-center w-50 p-1">{likeBtn}</td>
                    </tr>
                </table>
            </ul>
        );
    }

}

export default Card;