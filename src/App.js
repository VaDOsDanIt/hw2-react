import React, {Component} from 'react';
import CardList from './cardList'
import 'bootstrap/dist/css/bootstrap.min.css';


class App extends Component {
  constructor(props){
    super(props);
    this.openCardHandler = this.openCardHandler.bind(this);
    this.state = {
        cards: [],
        isOpenCard: false,
    }
  }

  componentDidMount(){
        fetch('/items.json')
            .then(response => response.json())
            .then(json => this.setState({cards: json}));
  }

  openCardHandler(){
      this.setState({
          ioOpenCard: !this.state.isOpenCard,
      })
  }

    render() {
        return (
            <div >
                <header className="card-header">
                    <button onClick={this.openCardHandler} className="btn btn-secondary">Корзина</button>
                </header>
                <CardList cards={this.state.cards}/>

                <footer className="card-footer"></footer>
            </div>
        );
    }
}

export default App;